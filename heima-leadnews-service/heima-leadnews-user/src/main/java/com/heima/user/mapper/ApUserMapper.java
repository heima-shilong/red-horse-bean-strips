package com.heima.user.mapper;

import com.heima.model.user.pojos.ApUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 10026
* @description 针对表【ap_user(APP用户信息表)】的数据库操作Mapper
* @createDate 2023-12-01 14:59:09
* @Entity com.heima.model.user.pojos.ApUser
*/
public interface ApUserMapper extends BaseMapper<ApUser> {

}




