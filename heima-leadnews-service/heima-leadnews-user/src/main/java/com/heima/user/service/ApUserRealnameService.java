package com.heima.user.service;

import com.heima.model.user.pojos.ApUserRealname;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 10026
* @description 针对表【ap_user_realname(APP实名认证信息表)】的数据库操作Service
* @createDate 2023-12-01 14:59:10
*/
public interface ApUserRealnameService extends IService<ApUserRealname> {

}
