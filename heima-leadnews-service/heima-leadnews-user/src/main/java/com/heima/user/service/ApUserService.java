package com.heima.user.service;

import com.heima.model.user.dtos.LoginDto;
import com.heima.model.user.dtos.LoginResDto;
import com.heima.model.user.pojos.ApUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 10026
* @description 针对表【ap_user(APP用户信息表)】的数据库操作Service
* @createDate 2023-12-01 14:59:09
*/
public interface ApUserService extends IService<ApUser> {

    LoginResDto loginAuth(LoginDto loginDto);
}
