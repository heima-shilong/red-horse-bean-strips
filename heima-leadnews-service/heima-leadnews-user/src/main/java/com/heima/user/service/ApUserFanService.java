package com.heima.user.service;

import com.heima.model.user.pojos.ApUserFan;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 10026
* @description 针对表【ap_user_fan(APP用户粉丝信息表)】的数据库操作Service
* @createDate 2023-12-01 14:59:09
*/
public interface ApUserFanService extends IService<ApUserFan> {

}
