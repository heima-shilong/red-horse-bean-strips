package com.heima.user.mapper;

import com.heima.model.user.pojos.ApUserFollow;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 10026
* @description 针对表【ap_user_follow(APP用户关注信息表)】的数据库操作Mapper
* @createDate 2023-12-01 14:59:10
* @Entity com.heima.model.user.pojos.ApUserFollow
*/
public interface ApUserFollowMapper extends BaseMapper<ApUserFollow> {

}




