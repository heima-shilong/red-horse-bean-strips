package com.heima.user.mapper;

import com.heima.model.user.pojos.ApUserRealname;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 10026
* @description 针对表【ap_user_realname(APP实名认证信息表)】的数据库操作Mapper
* @createDate 2023-12-01 14:59:10
* @Entity com.heima.model.user.pojos.ApUserRealname
*/
public interface ApUserRealnameMapper extends BaseMapper<ApUserRealname> {

}




