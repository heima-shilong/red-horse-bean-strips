package com.heima.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.user.pojos.ApUserFollow;
import com.heima.user.mapper.ApUserFollowMapper;
import com.heima.user.service.ApUserFollowService;
import org.springframework.stereotype.Service;

/**
 * @author 10026
 * @description 针对表【ap_user_follow(APP用户关注信息表)】的数据库操作Service实现
 * @createDate 2023-12-01 14:59:10
 */
@Service
public class ApUserFollowServiceImpl extends ServiceImpl<ApUserFollowMapper, ApUserFollow>
        implements ApUserFollowService {

}




