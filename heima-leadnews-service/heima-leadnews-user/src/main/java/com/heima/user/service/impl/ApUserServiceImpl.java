package com.heima.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.common.exception.CustomException;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.dtos.LoginDto;
import com.heima.model.user.dtos.LoginResDto;
import com.heima.model.user.pojos.ApUser;
import com.heima.user.mapper.ApUserMapper;
import com.heima.user.service.ApUserService;
import com.heima.utils.common.AppJwtUtil;
import com.heima.utils.common.MD5Utils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import static com.heima.model.common.enums.AppHttpCodeEnum.LOGIN_PWDANDUNAME_EMPTY;

/**
 * @author 10026
 * @description 针对表【ap_user(APP用户信息表)】的数据库操作Service实现
 * @createDate 2023-12-01 14:59:09
 */
@Service
public class ApUserServiceImpl extends ServiceImpl<ApUserMapper, ApUser>
        implements ApUserService {

    @Override
    public LoginResDto loginAuth(LoginDto loginDto) {
        // 获取用户名密码
        String password = loginDto.getPassword();
        String phone = loginDto.getPhone();

        LoginResDto loginResDto = new LoginResDto();
        //判断用户名密码是否为空
        if (StringUtils.isEmpty(password) && StringUtils.isEmpty(phone)) {
            // 用户名密码都为空为游客
            String token = AppJwtUtil.getToken(0L);
            loginResDto.setToken(token);
        } else {
            //正常登陆用户
            if (StringUtils.isEmpty(phone) || StringUtils.isEmpty(password)) {
                throw new CustomException(LOGIN_PWDANDUNAME_EMPTY);
            }
            //校验用户名 即 根据用户名从数据库查询用户信息
            LambdaQueryWrapper<ApUser> queryWrapper = new LambdaQueryWrapper<>();
            // 用户名 phone=用户名
            queryWrapper.eq(ApUser::getPhone, phone);

            // 用户状态 必须为正常 1
            queryWrapper.eq(ApUser::getStatus, 1);

            ApUser user = getOne(queryWrapper);

            if (user == null) {// 用户信息为空 提示用户 用户不存在
                throw new CustomException(AppHttpCodeEnum.AP_USER_DATA_NOT_EXIST);
            }

            // 获取用户的salt
            String salt = user.getSalt();
            // 拼接用户输入的密码与salt
            String encodeWithSalt = MD5Utils.encodeWithSalt(password, salt);
            if (!encodeWithSalt.equals(user.getPassword())) {
                throw new CustomException(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
            }
            // 登陆成功 准备返回值数据

            //设置user信息到dtpo对象中
            //数据脱敏
            user.setPassword("******");
            user.setSalt("666666");
            loginResDto.setUser(user);
            // 准备token
            String token = AppJwtUtil.getToken(user.getId());
            loginResDto.setToken(token);


        }

        return loginResDto;
    }
}




