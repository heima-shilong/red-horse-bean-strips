package com.heima.user.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.LoginDto;
import com.heima.model.user.dtos.LoginResDto;
import com.heima.user.service.ApUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/login/")
@Api(tags = "用户登录相关接口")
public class ApUserController {
    @Autowired
    private ApUserService apUserService;

    // {"phone":"12011223456","password":"888itcast.CN764%...","equipmentId":"88888888"}
    @PostMapping("/login_auth")
    @ApiOperation(value = "登陆接口")
    public ResponseResult<LoginResDto> loginAuth(@RequestBody LoginDto loginDto) {
        LoginResDto loginResDto=apUserService.loginAuth(loginDto);
        return ResponseResult.okResult(loginResDto);
    }
}
