package com.heima.user.mapper;

import com.heima.model.user.pojos.ApUserFan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 10026
* @description 针对表【ap_user_fan(APP用户粉丝信息表)】的数据库操作Mapper
* @createDate 2023-12-01 14:59:09
* @Entity com.heima.model.user.pojos.ApUserFan
*/
public interface ApUserFanMapper extends BaseMapper<ApUserFan> {

}




