package com.heima.user.service;

import com.heima.model.user.pojos.ApUserFollow;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 10026
* @description 针对表【ap_user_follow(APP用户关注信息表)】的数据库操作Service
* @createDate 2023-12-01 14:59:10
*/
public interface ApUserFollowService extends IService<ApUserFollow> {

}
