package com.heima.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.user.pojos.ApUserFan;
import com.heima.user.mapper.ApUserFanMapper;
import com.heima.user.service.ApUserFanService;
import org.springframework.stereotype.Service;

/**
 * @author 10026
 * @description 针对表【ap_user_fan(APP用户粉丝信息表)】的数据库操作Service实现
 * @createDate 2023-12-01 14:59:09
 */
@Service
public class ApUserFanServiceImpl extends ServiceImpl<ApUserFanMapper, ApUserFan>
        implements ApUserFanService {

}




