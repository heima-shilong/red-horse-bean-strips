package com.heima.model.user.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * APP用户信息表
 * @TableName ap_user
 */
@TableName(value ="ap_user")
public class ApUser implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 密码、通信等加密盐
     */
    private String salt;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名",required = true)
    private String name;

    /**
     * 密码,md5加密
     */
    @ApiModelProperty(value = "密码",required = true)
    private String password;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 头像
     */
    private String image;

    /**
     * 0 男
            1 女
            2 未知
     */
    private Integer sex;

    /**
     * 0 未
            1 是
     */
    private Integer isCertification;

    /**
     * 是否身份认证
     */
    private Integer isIdentityAuthentication;

    /**
     * 0正常
            1锁定
     */
    private Integer status;

    /**
     * 0 普通用户
            1 自媒体人
            2 大V
     */
    private Integer flag;

    /**
     * 注册时间
     */
    private Date createdTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 密码、通信等加密盐
     */
    public String getSalt() {
        return salt;
    }

    /**
     * 密码、通信等加密盐
     */
    public void setSalt(String salt) {
        this.salt = salt;
    }

    /**
     * 用户名
     */
    public String getName() {
        return name;
    }

    /**
     * 用户名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 密码,md5加密
     */
    public String getPassword() {
        return password;
    }

    /**
     * 密码,md5加密
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 手机号
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 手机号
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 头像
     */
    public String getImage() {
        return image;
    }

    /**
     * 头像
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * 0 男
            1 女
            2 未知
     */
    public Integer getSex() {
        return sex;
    }

    /**
     * 0 男
            1 女
            2 未知
     */
    public void setSex(Integer sex) {
        this.sex = sex;
    }

    /**
     * 0 未
            1 是
     */
    public Integer getIsCertification() {
        return isCertification;
    }

    /**
     * 0 未
            1 是
     */
    public void setIsCertification(Integer isCertification) {
        this.isCertification = isCertification;
    }

    /**
     * 是否身份认证
     */
    public Integer getIsIdentityAuthentication() {
        return isIdentityAuthentication;
    }

    /**
     * 是否身份认证
     */
    public void setIsIdentityAuthentication(Integer isIdentityAuthentication) {
        this.isIdentityAuthentication = isIdentityAuthentication;
    }

    /**
     * 0正常
            1锁定
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 0正常
            1锁定
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 0 普通用户
            1 自媒体人
            2 大V
     */
    public Integer getFlag() {
        return flag;
    }

    /**
     * 0 普通用户
            1 自媒体人
            2 大V
     */
    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    /**
     * 注册时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 注册时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ApUser other = (ApUser) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getSalt() == null ? other.getSalt() == null : this.getSalt().equals(other.getSalt()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getPassword() == null ? other.getPassword() == null : this.getPassword().equals(other.getPassword()))
            && (this.getPhone() == null ? other.getPhone() == null : this.getPhone().equals(other.getPhone()))
            && (this.getImage() == null ? other.getImage() == null : this.getImage().equals(other.getImage()))
            && (this.getSex() == null ? other.getSex() == null : this.getSex().equals(other.getSex()))
            && (this.getIsCertification() == null ? other.getIsCertification() == null : this.getIsCertification().equals(other.getIsCertification()))
            && (this.getIsIdentityAuthentication() == null ? other.getIsIdentityAuthentication() == null : this.getIsIdentityAuthentication().equals(other.getIsIdentityAuthentication()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getFlag() == null ? other.getFlag() == null : this.getFlag().equals(other.getFlag()))
            && (this.getCreatedTime() == null ? other.getCreatedTime() == null : this.getCreatedTime().equals(other.getCreatedTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getSalt() == null) ? 0 : getSalt().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        result = prime * result + ((getPhone() == null) ? 0 : getPhone().hashCode());
        result = prime * result + ((getImage() == null) ? 0 : getImage().hashCode());
        result = prime * result + ((getSex() == null) ? 0 : getSex().hashCode());
        result = prime * result + ((getIsCertification() == null) ? 0 : getIsCertification().hashCode());
        result = prime * result + ((getIsIdentityAuthentication() == null) ? 0 : getIsIdentityAuthentication().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getFlag() == null) ? 0 : getFlag().hashCode());
        result = prime * result + ((getCreatedTime() == null) ? 0 : getCreatedTime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", salt=").append(salt);
        sb.append(", name=").append(name);
        sb.append(", password=").append(password);
        sb.append(", phone=").append(phone);
        sb.append(", image=").append(image);
        sb.append(", sex=").append(sex);
        sb.append(", isCertification=").append(isCertification);
        sb.append(", isIdentityAuthentication=").append(isIdentityAuthentication);
        sb.append(", status=").append(status);
        sb.append(", flag=").append(flag);
        sb.append(", createdTime=").append(createdTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}