package com.heima.model.user.dtos;

import com.heima.model.user.pojos.ApUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LoginResDto {
    private ApUser user;
    @ApiModelProperty(value = "token",required = true)
    private String token;
}
